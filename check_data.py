#!/usr/bin/python

import sys

class Person():

	def __init__(self):
		pass

def find_schema(file_name, debug=False):
	line_no = 0
	schema = []
	f = open(file_name)
	for line in f.readlines():
		line_no += 1
		line = line.strip("\n")
		if not line.startswith("#"):
			continue
		arr = line.strip("# ").split(",")
		for i in range(0, len(arr)):
			schema.append(arr[i].strip())
		if debug:
			print("line %d -- schema found -- consists of %d fields (%s)" % (line_no, len(schema), schema))
	f.close()
	return schema

def find_entries(file_name, schema, debug=False):
	line_no = 0
	entries = []
	f = open(file_name)
	for line in f.readlines():
		line_no += 1
		line = line.strip("\n")
		if line.startswith("#"):
			continue
		arr = line.split(",")
		if not len(arr):
			continue
		fields = []
		for i in range(0, len(arr)):
			arr[i] = arr[i].strip()
			arr[i] = arr[i].strip('"')
			fields.append(arr[i])
		if len(fields) != len(schema):
			if debug:
				print("line %d -- entry format incomplete" % line_no)
			continue
		entries.append(fields)
	f.close()
	return entries

def print_entry(schema, fields):
	for i in range(0, len(schema)):
		print("%s: %s" % (schema[i], fields[i]))

def main():
	debug = False
	input_file = "Politiker.txt"
	schema = find_schema(input_file, debug)
	entries = find_entries(input_file, schema, debug)
	for fields in entries:
		print("-" * 72)
		print_entry(schema, fields)

if __name__ == '__main__':
	main()
